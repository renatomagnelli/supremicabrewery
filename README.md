# SupremicaBrewery

Model made for ENGF17 - Sistemas Discretos as taught on 2nd term 2019 of 
Universidade Federal da Bahia.

This project is the implementation of the Brewery problem as stated on the 
Teoria de Controle Supervisório de Sistemas a Eventos Discretos 
handout, 
presented on 
V Simpósio Brasileiro de Automação Inteligente 
by Prof. José Eduardo Ribeiro Cury, november 2001.

The state space is implemented on the 
[Supremica - Supervisory Control Tool](http://supremica.org/) software. 
The wmod extension file is a XML list of states and transitions that can be open 
and simulated on Supremica.

# State representation

State names merge the actuator state and the bottle state on the belt.

Bottle representation is [nnnn] where n is a number between 0 and 3 where 0 is no bottle, 1 is empty bottle, 2 is full bottle and 3 is full and capped bottle.

Actuator representation is [aaaaa] where a is the state of the actuator of corresponding position, 0 being OFF and 1 being ON.

Example:

S0100_01000: empty bottle on position P2 and pneumatic actuator turned on to place a bottle on position P1.

Event b1 will bring the previous state to S1100_00000, turning off actuator and confirming a bottle on P1.

# Limitations

Turning ON simultaneously the following states bring the system to a non-modelled condition and are forbidden:

* Conveyor belt (a0) and pneumatic actuator (a1) crash an empty bottle;
* Conveyor belt (a0) and pump (a2) drench belt in beer;
* Conveyor belt (a0) and capping machine (a3) leave cap on the belt or crash a bottle;
* Conveyor belt (a0) and robot (a4) crash a full bottle;

Operation of equipment with no bottle on the position, similarly, is forbidden:

* Pump (a2) will drench belt in beer;
* Capping machine (a3) will leave a cap on the belt;
* Robot (a4) will mess the output buffer;

Operating twice the same equipment on the same bottle, after finishing a previous operation is also forbidden:

* Conveyor belt (a0) will skip a step and bring a uncapped or empty bottle to output buffer;
* Pneumatic actuator (a1) will crash the first placed bottle by pushing a second one into it;
* Pump (a2) will overfill a bottle;
* Capping machine (a3) will likely crash a bottle;
* Robot (a4) will mess the output buffer;
